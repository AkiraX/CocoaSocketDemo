//
//  ViewController.m
//  SocketDemo
//
//  Created by apple on 15/6/3.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "ViewController.h"
#import <GCDAsyncSocket.h>

@interface ViewController () <GCDAsyncSocketDelegate>

@end

@implementation ViewController
{
    GCDAsyncSocket *socket;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    socket.userData = @"asdas";
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)connectServer {
    if ([socket connectToHost:@"192.168.0.136" onPort:2333 error:nil]) {
        
        NSLog(@"connect successed");
    }else {
        NSLog(@"connect failed");
    }
}

- (IBAction)didClickedConnect:(id)sender {
    
    [self connectServer];
    
}

#pragma mark - GCDAsyncSocketDelegate Methods

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    NSLog(@"did connect successed!!!");
}


@end
