//
//  ViewController.m
//  SocketServer
//
//  Created by apple on 15/6/3.
//  Copyright (c) 2015年 apple. All rights reserved.
//

#import "ViewController.h"
#import "GCDAsyncSocket.h"

@interface ViewController () <GCDAsyncSocketDelegate>

@end

@implementation ViewController
{
    GCDAsyncSocket *socket;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    
    if ([socket acceptOnPort:2333 error:&error]) {
       
        NSLog(@"accept successed!!");
        
    }else {
        
        NSLog(@"accept failed!!");
    }
    

    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GCDAsyncSocketDelegate Methods


- (void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket {
    
    NSLog(@"did accept :\n%@", newSocket.userData);
    
}

@end
